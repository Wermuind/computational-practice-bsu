## Tasks
---
1. Resource Booking:
a) [Application](https://book1ng.herokuapp.com)
b) [Code](https://bitbucket.org/Wermuind/computational-practice-bsu/src/master/Booking/start.py)
2. [Algorithmization part 1](https://bitbucket.org/Wermuind/computational-practice-bsu/src/master/Algorithmization_part_1.py)
3. [Algorithmization part 2](https://bitbucket.org/Wermuind/computational-practice-bsu/src/master/Algorithmization_part_2.py)
4. [Algorithmization part 3](https://bitbucket.org/Wermuind/computational-practice-bsu/src/master/Algorithmization_part_3.py)

