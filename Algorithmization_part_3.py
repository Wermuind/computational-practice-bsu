def index(List, num):
    for i in List:
        if i == num: return List.index(i)
    d = []
    for i in List:
        if num-i >= 0: d.append(num-i)
    if d == []: return 0
    d.sort()
    return (List.index(num-d[0]))+List.count(num-d[0])

#примеры из задания:
if index([1,3,5,6], 5) == 2: print(True)
if index([1,3,5,6], 2) == 1: print(True)
if index([1,3,5,6], 0) == 0: print(True)
#дополнительно:
if index([0,2,3,10,20,33], 35) == 6: print(True)
if index([0,5,9,10,11,20,33], 32) == 6: print(True)
# массивы, где есть несколько одинаковых элементов
if index([0,5,5,9,9,9,10,11,20,33], 32) == 9: print(True)
if index([0,5,5,9,9,9,10,11,20,20,33], 32) == 10: print(True)