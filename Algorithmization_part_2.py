def num_of_1s(x):
    result = []
    temp = []
    for i in range(0, x+1):
        temp.append(str(bin(i)))
    for i in temp:
        result.append(i.count("1", 2, len(i)))
    return result